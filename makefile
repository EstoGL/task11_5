task5: length.o calc.o
	gcc length.o -g -o length
	gcc calc.o -g -o calc
length.o: length.c 
	gcc -g -c length.c
calc.o: calc.c 
	gcc -g -c calc.c
length.c: length.y 
	yacc -g -o length.c length.y
calc.c: calc.y 
	yacc -g -o calc.c calc.y
clean:
	rm *.o 
	rm *.c
	rm *.dot
