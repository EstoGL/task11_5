%{

/*вычисление арифметического выражения*/

#include<stdio.h>

%}

%token DIGIT
%left '+' '-'
%left '*' '/'
%left '(' ')'
%left UNARY_MINUS UNARY_PLUS

%%
P: S { printf("\nThe answer is: %d\n",$$); return 0; } ;
S: S '+' S { $$ = $1 + $3; } |
   S '-' S { $$ = $1 - $3; } |
   S '*' S { $$ = $1 * $3; } |
   S '/' S { if($3) $$ = $1 / $3; else { yyerror(); return 1; } } |
   '(' S ')' { $$ = $2; } |
   '-' S %prec UNARY_MINUS { $$ = -$2; } |
   '+' S %prec UNARY_PLUS { $$ = $2; } |
   NUMBER { $$ = $1; } ;
NUMBER: NUMBER DIGIT { $$ = $1 * 10 + $2; }| DIGIT {$$ = $1;} ;
%%

void main() {
   printf("Input arithmetic expression: "); 
   yyparse();
}

yylex () {
    int sym = getchar ();
    while ((sym == ' ') || (sym == '\t')) sym = getchar();
    if ((sym == '+') || (sym == '-') || (sym == '*') || (sym == '/') || (sym == ')') || (sym == '(')) return sym;
    if ((sym >= '0') && (sym <= '9')) {
      yylval = sym - '0';
      return DIGIT;
    }
    if (sym == '\n') return 0;
    printf("\nUnknown symbol\n");
    exit(1);
}

int yyerror(const char* str) {
   printf("\nError\n\n");
}
